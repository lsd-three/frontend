import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

public class E2ETest {

    String SSIPadr;
    String IPadr;
    WebDriver driver;      


    @Before
    public void setup() throws Exception {

        // SSIPadr = System.getenv("SSIPADR"); // nope!
        SSIPadr = "http://167.71.63.78:4444/wd/hub";
        IPadr = "http://46.101.241.48:9090/frontend/";
        //place geckodriver in folder BEFORE running test locally (removed from folder due to gitlab restrictions)
        //System.setProperty("webdriver.gecko.driver", "/home/<USERNAME>/gecko/geckodriver"); 
        //driver = new FirefoxDriver();
        driver = new RemoteWebDriver(new URL(SSIPadr), DesiredCapabilities.firefox());
        
    }

    // maybe usefull later
    //takeSnapShot(driver, "/screens/test.png");
    public void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile = new File(fileWithPath);
        FileUtils.copyFile(SrcFile, DestFile);
    }

    @Test
    public void Navigate_To_FrontPage(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running Navigate_To_FrontPage");
        System.out.println("-------------------------------------------------");
        driver.get(IPadr);
        
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element = driver.findElement(By.className("header"));
        String actual = element.getAttribute("innerHTML");

        assertEquals("Faraday Car Rental Main Page!", actual);
        driver.quit();
    }

    @Test
    public void Navigate_To_AddNewBookingPage(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running Navigate_To_AddNewBookingPage");
        System.out.println("-------------------------------------------------");
        driver.get(IPadr);
        WebElement element = driver.findElement(By.id("fc-id"));
        element.click();
        
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        element = driver.findElement(By.className("header"));
        String actual = element.getAttribute("innerHTML");

        assertEquals("Find car", actual);
        driver.quit();
    }

    @Test
    public void Navigate_To_SearchByDriverIdPage(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running Navigate_To_SearchByDriverIdPage");
        System.out.println("-------------------------------------------------");
        
        driver.get(IPadr);
        WebElement element = driver.findElement(By.id("sbd-id"));
        element.click();
        
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        element = driver.findElement(By.className("header"));
        String actual = element.getAttribute("innerHTML");

        assertEquals("Find a booking", actual);
        driver.quit();
    }

    @Test
    public void Navigate_To_SearchByBookingIdPage(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running Navigate_To_SearchByBookingIdPage");
        System.out.println("-------------------------------------------------");
        
        driver.get(IPadr);
        WebElement element = driver.findElement(By.id("sbdbi-id"));
        element.click();
        
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        element = driver.findElement(By.className("header"));
        String actual = element.getAttribute("innerHTML");

        assertEquals("Find a booking", actual);
        driver.quit();
    }


    private String RunAddNewBookingPageOne(){
        //the wonderfull world of java dateTime <3
        String DATE_FORMAT_NOW = "yyyy-MM-dd'T'HH:mm";
        
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        
        driver.get(IPadr+"findCars.jsp");
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element = driver.findElement(By.name("pickUpTime"));
        element.click(); //click the date field before inputting values - only needed for date-type-fields
        element.sendKeys(sdf.format(cal.getTime()));

        element = driver.findElement(By.name("deliveryTime"));
        element.click(); //click the date field before inputting values - only needed for date-type-fields
        element.sendKeys(sdf.format(cal.getTime()));

        element = driver.findElement(By.name("pickUpPlace"));
        element.sendKeys("23");

        element.submit();

        wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        


        WebElement element2 = driver.findElement(By.className("header"));
        String actual = element2.getAttribute("innerHTML");
        return actual;
    }

    private String RunAddNewBookingPagetwo(){
        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        driver.findElement(By.linkText("Select")).click();       

        wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element = driver.findElement(By.className("header"));
        String actual = element.getAttribute("innerHTML");
        return actual;

    }
    private void RunAddNewBookingPageThree(String driverId){
        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element = driver.findElement(By.name("deliveryPlace"));
        element.sendKeys("23");

        element = driver.findElement(By.name("driverId"));
        element.sendKeys(driverId);

        element = driver.findElement(By.name("driverName"));
        element.sendKeys("SeleniumTestName");

        element = driver.findElement(By.name("driverBirthday"));
        element.clear();
        element.sendKeys("2001-09-11");

        element = driver.findElement(By.name("driverLicenseNumber"));
        element.sendKeys("FF-1234567890");
        
        element.submit();

        wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

    }

    @Test
    public void AddNewBookingPageOne()throws Exception{
        System.out.println("-------------------------------------------------");
        System.out.println("Running AddNewBookingPageOne");
        System.out.println("-------------------------------------------------");

        String actual = RunAddNewBookingPageOne();
        assertEquals("Find car result", actual);
        driver.quit();

    }
    
    @Test
    public void AddNewBookingPageTwo()throws Exception{
        System.out.println("-------------------------------------------------");
        System.out.println("Running AddNewBookingPageTwo");
        System.out.println("-------------------------------------------------");

        String actual = RunAddNewBookingPageOne();
        assertEquals("Find car result", actual);

        actual = RunAddNewBookingPagetwo();
        assertEquals("Add new booking", actual);

        driver.quit();
    }

    @Test
    public void AddNewBookingPageThree()throws Exception{
        System.out.println("-------------------------------------------------");
        System.out.println("Running AddNewBookingPageThree");
        System.out.println("-------------------------------------------------");

        String driverId = "54";
        String actual = RunAddNewBookingPageOne();
        assertEquals("Find car result", actual);

        actual = RunAddNewBookingPagetwo();
        //verify that we have landed on the correct page
        assertEquals("Add new booking", actual);

        //run all input and navigate to next page in booking process
        RunAddNewBookingPageThree(driverId);
        //assertEquals("Add booking result", actual);
        driver.quit();
    }


    @Test
    public void AddNewBookingVerifyPage()throws Exception{
        System.out.println("-------------------------------------------------");
        System.out.println("Running AddNewBookingVerifyPage");
        System.out.println("-------------------------------------------------");

        String driverId = "54";
        String actual = RunAddNewBookingPageOne();
        assertEquals("Find car result", actual);

        actual = RunAddNewBookingPagetwo();
        //verify that we have landed on the correct page
        assertEquals("Add new booking", actual);

        //run all input and navigate to next page in booking process
        RunAddNewBookingPageThree(driverId);
        //assertEquals("Add booking result", actual);


        WebElement element = driver.findElement(By.id("driver-id"));
        actual = element.getAttribute("innerHTML");
        
        assert(actual.contains(driverId));

        driver.quit();
    }


    @Test
    public void SearchBookingById(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running SearchBookingById");
        System.out.println("-------------------------------------------------");
        //first create the booking
        String driverId = "54";
        String orderId;
        String actual = RunAddNewBookingPageOne();
        //assertEquals("Find car result", actual);

        actual = RunAddNewBookingPagetwo();
        //verify that we have landed on the correct page
        //assertEquals("Add new booking", actual);

        //run all input and navigate to next page in booking process
        RunAddNewBookingPageThree(driverId);
        //assertEquals("Add booking result", actual);

        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("driver-id")));        


        WebElement element = driver.findElement(By.id("driver-id"));
        actual = element.getAttribute("innerHTML");
        //assert(actual.contains(driverId));
        System.out.println("------------------------------------------------------------");        
        System.out.println("Driver id line : " + actual);        
        System.out.println("------------------------------------------------------------");        
        
        element = driver.findElement(By.id("order-id"));
        orderId = element.getAttribute("innerHTML").split(" ")[8]; // get the orderid from the orderid line
        System.out.println("------------------------------------------------------------");        
        System.out.println("OrderId Found: " + orderId);        
        System.out.println("------------------------------------------------------------");        


        //verify newly created booking
        driver.get(IPadr+"showBookingDetailsById.jsp");
        wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        //enter newly created booking id
        WebElement element3 = driver.findElement(By.name("identifier"));
        element3.sendKeys(orderId);
        element3.submit();

        WebDriverWait wait2 = new WebDriverWait(driver, 12);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element4 = driver.findElement(By.name("identifier"));
        String IdFound = element4.getAttribute("value");

        assertEquals(orderId, IdFound);
        driver.quit();
    }


    @Test
    public void CancelBooking(){
        System.out.println("-------------------------------------------------");
        System.out.println("Running CancelBooking");
        System.out.println("-------------------------------------------------");

        //first create the booking
        String driverId = "54";
        String orderId;
        String actualp1 = RunAddNewBookingPageOne();
        //assertEquals("Find car result", actual);

        String actualp2 = RunAddNewBookingPagetwo();
        //verify that we have landed on the correct page
        //assertEquals("Add new booking", actual);

        //run all input and navigate to next page in booking process
        RunAddNewBookingPageThree(driverId);
        //assertEquals("Add booking result", actualp3);

        WebDriverWait wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("driver-id")));        


        WebElement element6 = driver.findElement(By.id("driver-id"));
        String actual = element6.getAttribute("innerHTML");

        System.out.println("------------------------------------------------------------");        
        System.out.println(actual);        
        System.out.println("------------------------------------------------------------");        
        //assert(actual.contains(driverId));
            
        element6 = driver.findElement(By.id("order-id"));
        orderId = element6.getAttribute("innerHTML").split(" ")[8]; // get the orderid from the orderid line
        System.out.println("------------------------------------------------------------");        
        System.out.println("OrderId Found: " + orderId);        
        System.out.println("------------------------------------------------------------");        

        //verify newly created booking
        driver.get(IPadr+"showBookingDetailsById.jsp");
        wait = new WebDriverWait(driver, 12);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        //enter newly created booking id
        WebElement element7 = driver.findElement(By.name("identifier"));
        element7.sendKeys(orderId);
        element7.submit();

        WebDriverWait wait2 = new WebDriverWait(driver, 12);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        WebElement element8 = driver.findElement(By.name("identifier"));
        String IdFound = element8.getAttribute("value");
        //booking found
        assertEquals(orderId, IdFound);

        //deleting the booking
        element8.submit();

        //verify booking has been deleted
        driver.get(IPadr+"showBookingDetailsById.jsp");
        WebDriverWait wait3 = new WebDriverWait(driver, 12);
        wait3.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        //enter previously created booking id
        WebElement element9 = driver.findElement(By.name("identifier"));
        element9.sendKeys(orderId);
        element9.submit();

        
        WebDriverWait wait4 = new WebDriverWait(driver, 12);
        wait4.until(ExpectedConditions.visibilityOfElementLocated(By.className("header")));        

        //assert apropriate user info
        WebElement element10 = driver.findElement(By.id("error-id"));
        String userMsg = element10.getAttribute("innerHTML");
        assert(userMsg.contains("Error"));

        driver.quit();
    }




}
