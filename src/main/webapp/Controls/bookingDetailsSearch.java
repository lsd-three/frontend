package Controls;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DTOs.BookingDetails;
import DTOs.DriverDetails;
import interfaces.IBooking;
import utils.BookingFactory;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;



/**
 * bookingDetailsSearch
 */
@WebServlet(name="bookingDetailsSearch", urlPatterns={"/searchBooking"})
public class bookingDetailsSearch extends HttpServlet{


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException, RemoteException{


        if(request.getParameter("identifier") == null){
            request.setAttribute("ErrMsg", "Driver license id field can not be empty");
            request.getRequestDispatcher("/showBookingDetails.jsp").forward(request, response); 
            return ;
        }

        if(request.getParameter("identifier").length() != 10){
            request.setAttribute("ErrMsg", "Driver license id must be 10 digits long");
            request.getRequestDispatcher("/showBookingDetails.jsp").forward(request, response);                 
            return;        
        }
        
        try {
            DriverDetails driver = new DriverDetails(request.getParameter("identifier"), null, new Date(), null);
            IBooking obj = (IBooking) Naming.lookup("//46.101.241.48:1099/Booking");
            request.setAttribute("bookingList", (List<DTOs.BookingDetails>) obj.searchBooking(driver));
            request.getRequestDispatcher("/WEB-INF/bookingDetailsSearch.jsp").forward(request, response); 
        } 
        catch (MalformedURLException|NotBoundException e) {
            request.setAttribute("ErrMsg", "Server error, try again");
            request.getRequestDispatcher("/showBookingDetails.jsp").forward(request, response); 
        }
        

    }
    
}