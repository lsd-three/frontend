package utils;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import interfaces.IBooking;
import utils.MockedInterfaces.BookingImpl;

/**
 * BookingFactory
 */
public class BookingFactory {

    public static IBooking create() throws RemoteException, NotBoundException, MalformedURLException{
        try {
            if (System.getenv("ENVIRONMENT") == null) {
                return Client.getIBooking();
            }else{
                return new BookingImpl();
            }

        }
        catch (RemoteException e) {
            System.out.println(e.detail.getMessage());
        }
        catch (NotBoundException e) {
            System.out.println(e.getMessage());
        }
        catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }        
        
        return null;
    }
}