package utils.MockedInterfaces;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import DTOs.AvailabilityDetails;
import DTOs.CarDetails;
import DTOs.CarTypeDetails;
import exceptions.GetAvailableCarsException;
import interfaces.ICars;

/**
 * CarImpl
 */
public class CarImpl implements ICars {

    private List<CarDetails> carDetails = new ArrayList<>();

    public CarImpl() {
        // CarDetails carDetails1 = new CarDetails("1", new CarTypeDetails("1", "Audi", 4, 1000.00), "AS4855");
        // CarDetails carDetails2 = new CarDetails("2", new CarTypeDetails("2", "Ferrari ", 4, 2500.00), "AS4395");
        // CarDetails carDetails3 = new CarDetails("3", new CarTypeDetails("3", "Tesla", 4, 1500.00), "AS5097");
        // carDetails.add(carDetails1);
        // carDetails.add(carDetails2);
        // carDetails.add(carDetails3);
    }

    @Override
    public List<CarDetails> getAvailableCars(AvailabilityDetails availabilityDetails)
            throws GetAvailableCarsException, RemoteException {
        return carDetails;
    }

    
}