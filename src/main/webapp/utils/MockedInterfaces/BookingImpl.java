package utils.MockedInterfaces;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DTOs.NewBookingDetails;
import FEDTOs.CarIdentifier;
import FEDTOs.StationIdentifier;
import interfaces.IBooking;


import exceptions.AddNewBookingException;
import exceptions.GetSpecificBookingException;
import exceptions.SearchBookingException;

import DTOs.Identifiers.BookingIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.BookingDetails;

/**
 * BookingImpl
 */
public class BookingImpl implements IBooking {

    static List<BookingDetails> resultList = new ArrayList<BookingDetails>() {

        {
            add(new BookingDetails(new Date(), new Date(), new FEDTOs.StationIdentifier("1"), new FEDTOs.StationIdentifier("2"),  new FEDTOs.DriverIdentifier("1"), new FEDTOs.CarIdentifier("1"), 1000.0, "1"));
            add(new BookingDetails(new Date(), new Date(), new FEDTOs.StationIdentifier("1"), new FEDTOs.StationIdentifier("2"),  new FEDTOs.DriverIdentifier("1"), new FEDTOs.CarIdentifier("1"), 1000.0, "2"));
            add(new BookingDetails(new Date(), new Date(), new FEDTOs.StationIdentifier("1"), new FEDTOs.StationIdentifier("2"),  new FEDTOs.DriverIdentifier("1"), new FEDTOs.CarIdentifier("1"), 1000.0, "3"));
            add(new BookingDetails(new Date(), new Date(), new FEDTOs.StationIdentifier("1"), new FEDTOs.StationIdentifier("2"),  new FEDTOs.DriverIdentifier("1"), new FEDTOs.CarIdentifier("1"), 1000.0, "4"));
        }
    };
 
    @Override
    public void cancelBooking(BookingIdentifier arg0) throws RemoteException {
        resultList.removeIf(x -> x.getBookingId().equals(arg0.getBookingId()));
    }

    @Override
    public BookingDetails addNewBooking(NewBookingDetails newBookingDetails) throws AddNewBookingException, RemoteException {
        return new BookingDetails(
                new Date(),
                new Date(),
                new StationIdentifier("1"),
                new StationIdentifier("2"),
                new FEDTOs.DriverIdentifier("3"),
                new CarIdentifier("1"),
                200.0,
                "1"
        );
    }

    @Override
    public List<BookingDetails> searchBooking(DriverIdentifier arg0) throws SearchBookingException, RemoteException {
        return resultList;
    }

    @Override
    public BookingDetails getSpecificBooking(BookingIdentifier arg0)
            throws GetSpecificBookingException, RemoteException {

        return new FEDTOs.BookingDetails(new Date(), new Date(), new FEDTOs.StationIdentifier("1"),
                new FEDTOs.StationIdentifier("2"), new FEDTOs.DriverIdentifier("1"), new FEDTOs.CarIdentifier("1"),
                1000.00, arg0.getBookingId());
    }


}