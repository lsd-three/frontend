package utils;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import interfaces.ICars;
import utils.MockedInterfaces.CarImpl;

/**
 * CarFactory
 */
public class CarFactory {

    public static ICars create() throws RemoteException, NotBoundException, MalformedURLException{
        try {
            if (System.getenv("ENVIRONMENT") == null) {
                return Client.getICars();
            }else{
                return new CarImpl();
            }
        }
        catch (RemoteException e) {
            System.out.println(e.detail.getMessage());
        }
        catch (NotBoundException e) {
            System.out.println(e.getMessage());
        }
        catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        }        
        return null;

    }
}