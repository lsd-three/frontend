<!DOCTYPE>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fareday</title>

<style>
  *{
    margin: 0;
    padding: 0;
  }
  html{
    height: 100%
  }

  body{
    font-family:  "Helvetica Neue", Helvetica, Arial, sans-serif;
    
    display: flex;
    background: #DCDCDC;
    height: 100%;
  }

  aside{
    background: #1e88e5;
    color: #FFF;
    width: 200px;
    box-shadow: -10px 0 10px 10px rgba(0,0,0,.4)
  }
  aside h1{
    text-align: center;
    padding: 15px;
  }

  aside ul{
    list-style: none;
    padding-top: 15px;
  }

  aside a{
    color: #fff;
    text-decoration: none;
    padding: 5px 15px;
    display: block
  }
  aside a:hover{
    background: rgba(0,0,0,.1);
  }
  main {
    padding: 15px 20px;
  }
  main .header{
    text-decoration: underline;
    font-size: 1.5em;
    font-weight: bold;
    padding-bottom: 10px;
  }
  .booking{
      margin: 10px 10px 0 0; 
      background-color:#c0c0c0;
      width:450px;
      padding:16px;
  }
  button, input[type="submit"]{
    margin-top: 5px;
    background: #009688;
    color: #fff;
    font-weight: bold;
    font-size: 1.1em;
    padding: 10px 15px;
    border: none;
    border-radius: 50px;
    box-shadow: 2px 2px 5px rgba(0,0,0,.2);
  }
</style>
</head>
<body>

<aside>
    <h1>
     <a href="/frontend">Fareday</a>
    </h1>
    <ul>
      <li><a id="fc-id" href="findCars.jsp">Add new booking</a></li>
      <li><a id="sbd-id" href="showBookingDetails.jsp">Search by driver id</a></li>
      <li><a id="sbdbi-id" href="showBookingDetailsById.jsp">Search by booking id</a></li>
    </ul>
  </aside>
  <main>






