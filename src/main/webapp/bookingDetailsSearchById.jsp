<jsp:include page="inc_header.jsp" />  

<%@ page import="DTOs.Identifiers.BookingIdentifier" %>
<%@ page import="DTOs.Identifiers.DriverIdentifier" %>
<%@ page import="DTOs.BookingDetails" %>
<%@ page import="DTOs.DriverDetails" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="interfaces.IBooking" %>
<%@ page import="utils.BookingFactory" %>

<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.rmi.Naming" %>
<%@ page import="java.rmi.NotBoundException" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.Date" %>
<%@ page import="exceptions.SearchBookingException" %>




<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="header">Searchresult for booking with ID: <%= request.getParameter("identifier") %></div>
<%

try{

    IBooking client = (IBooking) Naming.lookup("//46.101.241.48:1099/Booking");

    DTOs.BookingDetails booking = (DTOs.BookingDetails) client.getSpecificBooking(new BookingDetails( new Date(), new Date(), null, null, null, null, 0.0, String.valueOf(request.getParameter("identifier"))));

    %>

        <div class="booking">
            <form action="removeBooking.jsp" method = "POST">
                <input type="hidden" name="identifier" value="<%= booking.getBookingId().toString() %>">
                <table>
                    <tr><td>Booking id:</td><td><%= booking.getBookingId().toString() %></td></tr>
                    <tr><td>Driver id:</td><td><%=  booking.getDriver().getId().toString()  %></td></tr>
                    <tr><td>Pickup station id:</td><td><%= booking.getPickUpPlace().getId().toString() %></td></tr>
                    <tr><td>Delivery station id:</td><td><%= booking.getDeliveryPlace().getId().toString() %></td></tr>
                    <tr><td>From:</td><td><%= booking.getPickUpTime().toString() %></td></tr>
                    <tr><td>To:</td><td><%= booking.getDeliveryTime().toString() %></td></tr>
                </table>
                <input type = "submit" value = "Cancel booking" />
            </form>
        </div>

    <%

}

catch(SearchBookingException e){
    out.println("<h3 id='error-id'>Error : "+e.getMessage()+"</h3>");
}
catch(Exception e){
    out.println("<h3 id='error-id'>Error : "+e.getMessage()+"</h3>");
}



%>

<jsp:include page="inc_footer.jsp" />