<jsp:include page="inc_header.jsp" />  

<%@ page import="DTOs.Identifiers.BookingIdentifier" %>
<%@ page import="DTOs.Identifiers.DriverIdentifier" %>
<%@ page import="DTOs.BookingDetails" %>
<%@ page import="DTOs.DriverDetails" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="interfaces.IBooking" %>
<%@ page import="utils.BookingFactory" %>

<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.rmi.Naming" %>
<%@ page import="java.rmi.NotBoundException" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.Date" %>
<%@ page import="exceptions.SearchBookingException" %>




<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="header">List of all bookings for driver with ID: <%= request.getParameter("identifier") %></div>
<%

try{

    DTOs.DriverDetails driver = new DTOs.DriverDetails(String.valueOf(request.getParameter("identifier")), null, new Date(), null);
    IBooking client = (IBooking) Naming.lookup("//46.101.241.48:1099/Booking");
    List<BookingDetails> searchBooking = (List<DTOs.BookingDetails>) client.searchBooking(driver);

    out.println("Number of bookings found: " + searchBooking.size());

    for (DTOs.BookingDetails var : searchBooking) 
    { 
    %>

        <div class="booking">
            <form action="removeBooking.jsp" method = "POST">
                <input type="hidden" name="identifier" value="<%= var.getBookingId().toString() %>">
                <table>
                    <tr><td>Booking id:</td><td><%= var.getBookingId().toString() %></td></tr>
                    <tr><td>Driver id:</td><td><%=  var.getDriver().getId().toString()  %></td></tr>
                    <tr><td>Pickup station id:</td><td><%= var.getPickUpPlace().getId().toString() %></td></tr>
                    <tr><td>Delivery station id:</td><td><%= var.getDeliveryPlace().getId().toString() %></td></tr>
                    <tr><td>From:</td><td><%= var.getPickUpTime().toString() %></td></tr>
                    <tr><td>To:</td><td><%= var.getDeliveryTime().toString() %></td></tr>
                </table>
                <input type = "submit" value = "Cancel booking" />
            </form>
        </div>

    <%
    }


}

catch(SearchBookingException e){
    out.println("<h3>Error : "+e.getMessage()+"</h3>");
}
catch(Exception e){
    out.println("<h3>Error : "+e.getMessage()+"</h3>");
}

%>

<jsp:include page="inc_footer.jsp" />