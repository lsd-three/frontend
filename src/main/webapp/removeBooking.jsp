<%@ page import="interfaces.IBooking" %>
<%@ page import="DTOs.Identifiers.BookingIdentifier" %>
<%@ page import="DTOs.BookingDetails" %>
<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.rmi.Naming" %>
<%@ page import="java.rmi.NotBoundException" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.Date" %>
<%

try{
    IBooking client = (IBooking) Naming.lookup("//46.101.241.48:1099/Booking");
    DTOs.BookingDetails bookingId = new DTOs.BookingDetails(new Date(), new Date(),null, null, null, null, 0,  String.valueOf(request.getParameter("identifier")));    
    client.cancelBooking(bookingId);

    response.sendRedirect("removeBookingConfirm.jsp");


}
catch(Exception e){
    out.println("<h3>Error : "+e.getMessage()+"</h3>");
}

%>

