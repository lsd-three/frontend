<jsp:include page="inc_header.jsp" />  

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="header">Find car</div>
<form action="findCarsResult.jsp" method="GET">
    <table>
        <tr>
            <td>Enter pick up time:</td>
            <td><input type="datetime-local" name="pickUpTime"/></td>
        </tr>
        <tr>
            <td>Enter delivery time:</td>
            <td><input type="datetime-local" name="deliveryTime"/></td>
        </tr>
        <tr>
            <td>Enter pick up station:</td>
            <td><input type="text" name="pickUpPlace" /></td>
        </tr>
        <tr>
            <td><input type="submit" value="Search"/></td>
            <td></td>
        </tr>
    </table>

</form>

<jsp:include page="inc_footer.jsp" />