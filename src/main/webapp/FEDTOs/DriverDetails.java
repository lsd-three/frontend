package FEDTOs;

import java.util.Date;

/**
 * DriverDetails
 */
public class DriverDetails extends DTOs.DriverDetails {


    public DriverDetails(String id, String name, Date age, String driverLicenseNumber) {
        super(id, name, age, driverLicenseNumber);
    }

}