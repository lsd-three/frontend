package FEDTOs;

import java.util.Date;

import DTOs.Identifiers.CarIdentifier;
import DTOs.Identifiers.DriverIdentifier;
import DTOs.Identifiers.StationIdentifier;

/**
 * BookingInfo
 */
public class BookingDetails extends DTOs.BookingDetails {


    public BookingDetails(Date pickUpTime, Date deliveryTime, StationIdentifier pickUpPlace,
            StationIdentifier deliveryPlace, DriverIdentifier driver, CarIdentifier car, double totalPrice,
            String bookingId) {
        super(pickUpTime, deliveryTime, pickUpPlace, deliveryPlace, driver, car, totalPrice, bookingId);
    }
}