<jsp:include page="inc_header.jsp" />
<%@ page import="java.util.List" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="DTOs.AvailabilityDetails" %>
<%@ page import="DTOs.HotelDetails" %>
<%@ page import="DTOs.CarDetails" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@ page import="exceptions.GetAvailableCarsException" %>

<%@ page import="utils.CarFactory" %>
<%@ page import="interfaces.ICars" %>
<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.rmi.Naming" %>
<%@ page import="java.rmi.NotBoundException" %>
<%@ page import="java.rmi.RemoteException" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="header">Find car result</div>
<%

String pickUpTime = request.getParameter("pickUpTime");
String deliveryTime = request.getParameter("deliveryTime");
String pickUpPlace = request.getParameter("pickUpPlace");

DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");


try {
    AvailabilityDetails availabilityDetails = new AvailabilityDetails(new SimpleDateFormat("yyyy-MM-dd").parse(pickUpTime), new SimpleDateFormat("yyyy-MM-dd").parse(deliveryTime), new DTOs.HotelDetails(pickUpPlace, "","",0,null,""));

    ICars carClient = (ICars) Naming.lookup("//46.101.241.48:1099/Cars");


    List<DTOs.CarDetails> listOfCarDetails = (List<DTOs.CarDetails>)carClient.getAvailableCars(availabilityDetails);

    session.setAttribute("listOfCars", listOfCarDetails);
    %>
    <div>
        <h1>This is our car results:</h1>
        <table>
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Name</th>
                <th>License plate</th>
                <th>Number of seats</th>
                <th>Price per day</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (CarDetails carDetail : listOfCarDetails) {
                    out.println("<tr>");
                    out.println("<td>" + "<a href=\"addNewBooking.jsp?car=" + carDetail.getId() + "&pickUpTime=" + pickUpTime + "&deliveryTime=" + deliveryTime + "&pickUpPlace=" + pickUpPlace + "\">" + "Select" + "</a>" + "</td>");
                    out.println("<td>" + carDetail.getId() + "</td>");
                    out.println("<td>" + carDetail.getCarType().getName() + "</td>");
                    out.println("<td>" + carDetail.getLicensePlate() + "</td>");
                    out.println("<td>" + carDetail.getCarType().getNumberOfSeats() + "</td>");
                    out.println("<td>" + carDetail.getCarType().getPricePerDay() + "</td>");
                    out.println("</tr>");
                };
            %>
            </tbody>
        </table>
    </div>
    <%
}catch(GetAvailableCarsException e){
    out.println("<h3>Either no station with the provided id, or no cars available at the choosen station - " + e.getMessage() + "</h3>" );
}
%>
   
<jsp:include page="inc_footer.jsp" />