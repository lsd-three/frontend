<jsp:include page="inc_header.jsp" />
<%@ page import="DTOs.BookingDetails" %>
<%@ page import="DTOs.HotelDetails" %>
<%@ page import="DTOs.DriverDetails" %>
<%@ page import="DTOs.Identifiers.StationIdentifier" %>
<%@ page import="DTOs.NewBookingDetails" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="interfaces.IBooking" %>
<%@ page import="java.util.List" %>
<%@ page import="DTOs.CarDetails" %>
<%@ page import="java.net.MalformedURLException" %>
<%@ page import="java.rmi.Naming" %>
<%@ page import="java.rmi.NotBoundException" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="exceptions.AddNewBookingException" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="header">Add booking result</div>

<%
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");

    try{

        String pickUpTime = session.getAttribute("pickupTime").toString();
        String deliveryTime = session.getAttribute("deliveryTime").toString();
        String pickUpPlace = session.getAttribute("pickUpPlace").toString();
        String car = session.getAttribute("car").toString();
        List<CarDetails> listOfCars = (List<CarDetails>) session.getAttribute("listOfCars");
        String deliveryPlace = request.getParameter("deliveryPlace");

        String driverId = request.getParameter("driverId");
        String driverName = request.getParameter("driverName");
        String driverBirthday = request.getParameter("driverBirthday");
        String driverLicenseNumber = request.getParameter("driverLicenseNumber");

        DriverDetails driverDetails = new DriverDetails(driverId, driverName, formatterDate.parse(driverBirthday), driverLicenseNumber);

        NewBookingDetails newBookingDetails = new NewBookingDetails();
        newBookingDetails.setPickUpTime(formatter.parse(pickUpTime));
        newBookingDetails.setDeliveryTime(formatter.parse(deliveryTime));

        newBookingDetails.setPickUpPlace(   new DTOs.HotelDetails(pickUpPlace, "","",0,null,"")    );
        newBookingDetails.setDeliveryPlace(  new DTOs.HotelDetails(deliveryPlace, "","",0,null,"")  );

        CarDetails chosenCar = null;
            for (int i = 0; i < listOfCars.size(); i++) {
                if(listOfCars.get(i).getId().equals(car)){
                    chosenCar = listOfCars.get(i);
                }
            }
        if(chosenCar == null){
            %>
            <p>Choosen car not found</p>
            <%
            throw new Exception("Car is null");
        }
        

        newBookingDetails.setDriver(driverDetails);

        newBookingDetails.setCar(chosenCar);
        IBooking client = (IBooking) Naming.lookup("//46.101.241.48:1099/Booking");
        BookingDetails bookingDetails = client.addNewBooking(newBookingDetails);
%>

        <p id="order-id">Order has been made with the id: <b> <%=bookingDetails.getBookingId()%> </b></p>
        <p id="driver-id">With driver id: <b> <%=bookingDetails.getDriver().getId() %> </b></p>

        <p id="totalprice-id">Order total price will be: <b> <%=bookingDetails.getTotalPrice()%> </b></p> 
<%
    }
    catch(NumberFormatException e){
        out.println("<h3>Please enter digits only in driver id field</h3>");
    }
    catch(AddNewBookingException e){
        out.println("<h3>Error : "+e.getMessage()+"</h3>");
    }
    catch(Exception e){
        out.println("<h3>Error : "+e.getMessage()+"</h3>");
    }

%>


<jsp:include page="inc_footer.jsp" />