<jsp:include page="inc_header.jsp" />
<%@ page import="DTOs.CarDetails" %>
<%@ page import="java.util.List" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="header">Add new booking</div>

<%
    String car = request.getParameter("car");
    String pickUpTime = request.getParameter("pickUpTime");
    String deliveryTime = request.getParameter("deliveryTime");
    String pickUpPlace = request.getParameter("pickUpPlace");
    List<CarDetails> listOfCars = (List<CarDetails>) session.getAttribute("listOfCars");
    session.setAttribute("pickupTime", pickUpTime);
    session.setAttribute("deliveryTime", deliveryTime);
    session.setAttribute("pickUpPlace", pickUpPlace);
    session.setAttribute("car", car);


    CarDetails chosenCar = null;
    for (int i = 0; i < listOfCars.size(); i++) {
        if(listOfCars.get(i).getId().equals(car)){
            chosenCar = listOfCars.get(i);
        }
    }

%>


<div>
    <form action="addBookingResult.jsp" method="get">
        <table>
            <tr>
                <td>Pick up time:</td>
                <td><% out.println("<input type=\"datetime-local\" name=\"pickUpTime\" disabled value=\"" + pickUpTime + "\"/>"); %></td>
            </tr>
            <tr>
                <td>Delivery time:</td>
                <td><% out.println("<input type=\"datetime-local\" name=\"deliveryTime\" disabled value=\"" + deliveryTime + "\"/>"); %></td>
            </tr>
            <tr>
                <td>Pick up station:</td>
                <td><% out.println("<input type=\"text\" name=\"pickUpPlace\" disabled value=\"" + pickUpPlace + "\"/>"); %></td>
            </tr>
            <tr>
                <td>Delivery station:</td>
                <td><input type="text" name="deliveryPlace" /></td>
            </tr>
            <tr>
                <td>Driver id:</td>
                <td><input type="number" name="driverId" placeholder="Optional" /></td>
            </tr>
            <tr>
                <td>Driver name:</td>
                <td><input type="text" name="driverName" /></td>
            </tr>
            <tr>
                <td>Driver birthday:</td>
                <td><input type="datetime-local" name="driverBirthday" /></td>
            </tr>
            <tr>
                <td>Driver license number:</td>
                <td><input type="text" name="driverLicenseNumber" /></td>
            </tr>
            <tr>
                <td>Car:</td>
                <td><% out.println("<input type=\"text\" name=\"car\" disabled value=\"" + chosenCar.getCarType().getName() + "\"/>"); %></td>
            </tr>
            <tr>
                <td><input type="submit" value="Add Booking"/></td>
                <td></td>
            </tr>
        </table>        

    </form>
</div>

<jsp:include page="inc_footer.jsp" />